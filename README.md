![Build Status](https://gitlab.com/crossref/posi/badges/master/pipeline.svg)

---

This is the source for generating the website for ["The Principles of Open Scholarly Infrastructure"](https://openscholarlyinfrastructure.org/)

- It is hosted on Gitlab.
- It is generated using [Hugo](https://gohugo.io/)
- It is deployed automatically using GitLab CI

---
