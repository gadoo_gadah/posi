---
title: Posse
subtitle: Who has committed to the POSI principles?
date: "2021-06-11"
publishdate: "2021-03-09"
comments: false
---

These organizations have formally adopted the POSI principles by publishing an initial self-assessment, and committed to demonstrating evidence of following POSI in practice and routinely:

- [Crossref](https://www.crossref.org/blog/crossrefs-board-votes-to-adopt-the-principles-of-open-scholarly-infrastructure), last updated 2020-December-02
- [Dryad](https://blog.datadryad.org/2020/12/08/dryads-commitment-to-the-principles-of-open-scholarly-infrastructure/), last updated 2020-December-08
- [ROR](https://ror.org/blog/2020-12-16-aligning-ror-with-posi/), last updated 2020-December-16
- [JOSS](https://blog.joss.theoj.org/2021/02/JOSS-POSI), last updated 2021-February-14
- [OurResearch](https://blog.ourresearch.org/posi/), last updated 2021-June-10
- [OpenCitations](https://opencitations.wordpress.com/2021/08/09/opencitations-compliance-with-the-principles-of-open-scholarly-infrastructure/), last updated 2021-August-09
- [DataCite](https://doi.org/10.5438/vy7h-g464), last updated 2021-August-30

---

These organizations have referenced or built apon the POSI principles:

- COAR-SPARC's [Good Practice Principles for Scholarly Communication Services](https://sparcopen.org/our-work/good-practice-principles-for-scholarly-communication-services/)
- more to come...

---

If you spot something missing (there are definitely lots more) please open a [GitLab issue](https://gitlab.com/crossref/posi/-/issues) to add to the list.
